<?php
include_once("vendor/autoload.php");
use \App\category\animal\cat;
use \App\category\animal\dog;
use \App\category\stationary\book;
use \App\category\stationary\pencil;

$white_cat = new cat(10,"white",120);
$white_cat->name = "Mini";
echo $white_cat->name;
echo "<br>".$white_cat->color;
$white_cat->my_age(20);

$black_dog = new dog(2,"black",120);
$black_dog->name = "Sun";
echo "<br>".$black_dog->name;
echo "<br>".$black_dog->saySomething();
echo $black_dog->weight;

$book1 = new book("X",200,300);
$book1->name = "Sociology Book";
echo "<br>".$book1->name;
echo $book1->summary();
echo $book1->read();
echo $book1->price;

$pencil = new pencil("blue",20,100);
$pencil->name = "HB pencil";
echo "<br>".$pencil->name;
echo "<br>".$pencil->color;
echo "<br>".$pencil->doSomething();
echo $pencil->write();
echo $pencil->price;



?>