<?php
namespace App\category\stationary;

class book {
    public $name = "default name";
    public $author = "Mr. X";
    public $page_num = 210;
    public $price = 500;
    
    public function __construct($author , $page_num , $price){
        $this->author = $author;
        $this->page_num = $page_num;
        $this->price = $price;
    }
    
    public function summary(){
        echo "<br>". "I am a Book";
    }
    
    public function read(){
        echo "<br>". "I can read";
    }
}


?>