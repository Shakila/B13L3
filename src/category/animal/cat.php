<?php
namespace App\category\animal;

class cat {
    public $name = "default name";
    public $age = "2 year";
    public $color = "white";
    public $weight = "15 gram";
    
    public function __construct($age , $color , $weight){
        $this->age = $age;
        $this->color = $color;
        $this->weight = $weight;
    }
    
    public function saySomething(){
        echo "<br>". "I am a cat";
    }
    public function my_age($age){
        echo "<br>".$this->age = $age;
    }
}


?>