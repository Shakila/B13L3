<?php
namespace App\category\animal;

class dog {
    public $name = "default name";
    public $age = "2 year";
    public $color = "black";
    public $weight = "15 gram";
    
    public function __construct($age , $color , $weight){
        $this->age = $age;
        $this->color = $color;
        $this->weight = $weight;
    }
    
    public function saySomething(){
        echo "<br>". "I am a dog";
    }
    
    public function my_age($age){
        echo "<br>".$this->age = $age;
    }
}


?>